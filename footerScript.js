$('document').ready(function () {
	// Parse the URL
	alert('ready')
	function getParameterByName(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
	// Give the URL parameters variable names  
	var orderId = getParameterByName('orderid');
	var gclid = getParameterByName('gclid');

	//Replace values on page
	if (orderId) {
		$("body").html($("body").html().replace(/\{\{orderId\}\}/g, orderId));
	}


	if (gclid) {
		//console.log('gclid ', gclid);
		$("body").html($("body").html().replace(/\{\{gclid\}\}/g, gclid));
	};
});
